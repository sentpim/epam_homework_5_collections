package demo;

import service.StringService;
import service.StringServiceStream;

public class DemoService implements IDemoService {

    @Override
    public void execute() {
        String text = "hung up for sportion,\n" +
                "on amorous looking-glass;\n" +
                "I, that love's majesty\n" +
                "To strut before a wanton amorous wrinkled for made glorious summer by this fair proportive tricks,\n" +
                "Our steeds\n" +
                "To frightful marches to delight the winter of this wreaths;\n" +
                "Our bruised arms hung up for monuments;\n" +
                "Our brows bound with victorious pleasing nymph;\n" +
                "I, that am rudely stamp'd, and with victorious sun of mounting of a lady's changed to merry meeting barded stern alarums chamber\n" +
                "To fright the winter of our discontent\n" +
                "Made glorious pleasing of fearful marches to delight the souls of mounting of fearful adversaries,\n" +
                "He capers nimbly in a lady's chamber\n" +
                "To strut before a want love's majesty\n" +
                "To strut before a want lour'd upon our house\n" +
                "In the deep bosom of mounting of fearful adversaries,\n" +
                "He capers nimbly in a lute.\n" +
                "But I, that am not shaped front;\n" +
                "And all the lascivious looking-glass;\n" +
                "I, that am curtail'd of mountings,\n" +
                "Our brows bound war hath smooth'd his wreaths;\n" +
                "Our steeds\n" +
                "To strut before a w";

        StringService repWord = new StringService();
        StringServiceStream stream = new StringServiceStream();

        System.out.println("Кол-во каждого слова " + repWord.repeatWord(text));
        System.out.println("Со стримами " + stream.repeatWithStream(text));
        System.out.println("Уникальные слова " + repWord.uniqueWord(text));
        System.out.println("Со стримами " + stream.uniqueWordWithStream(text));
        System.out.println("Сортировка " + repWord.sortedWords(text));
        System.out.println("Со стримами " + stream.sortedWordsWithStream(text));
    }
}
