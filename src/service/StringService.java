package service;

import java.util.*;

public class StringService {

    public Map<String, Long> repeatWord(String text) {
        String[] stringArray = text.split("[^a-zA-Zа-яА-Я0-9]+");

        Map<String, Long> counter = new HashMap<>();
        for (String i : stringArray) {
            counter.put(i, (long) Collections.frequency(Arrays.asList(stringArray), i));
        }
        return counter;
    }

    public Set<String> uniqueWord(String text) {
        List<String> words = Arrays.asList(text.split("[^a-zA-Zа-яА-Я0-9]+"));
        return new HashSet<>(words);
    }

    public List<String> sortedWords(String text) {
        List<String> words = new ArrayList<>(uniqueWord(text));
        Comparator<String> ALPHABETICAL_ORDER = (str1, str2) -> {
            int res = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
            return (res != 0) ? res : str1.compareTo(str2);
        };
        words.sort(ALPHABETICAL_ORDER);
        return words;
    }
}
